-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: Testing_fullstack
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2018_07_25_042515_create_realestate_table',2),(9,'2018_07_25_043006_create_realestate_features_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('1956465e3e7f791a5d2abe7b223e66912778babf4e49308b02396e4252bc691e9f056a7529cb91d6',1,3,NULL,'[]',0,'2018-06-09 04:21:05','2018-06-09 04:21:05','2019-06-08 23:21:05'),('24447114125c0f918cb2468f55dba33204918fc2b607070ca9679239e7fb173506f4ce2651a7f31c',1,3,NULL,'[]',0,'2018-06-09 07:44:07','2018-06-09 07:44:07','2019-06-09 02:44:07'),('2c805e5e30713a824782b7d5425246508937aaf4903ee9481634cc1ed50e1b8cb00a32c5b26b50b8',1,3,NULL,'[]',0,'2018-06-09 18:23:30','2018-06-09 18:23:30','2018-06-10 18:23:30'),('2c9f78306228ddaa593eba4f8847e38b3a9b0e22bf2a11afce13e8d5dfef2bf31259fe19b7ceea06',2,3,NULL,'[]',0,'2018-06-09 20:50:19','2018-06-09 20:50:19','2018-06-10 20:50:19'),('2f91be97de5f7efa647f9edfa419dbc6637d82f288960b382c3f30329cea5412500c9cf6e857ad68',1,3,NULL,'[]',0,'2018-06-09 20:39:16','2018-06-09 20:39:16','2018-06-10 20:39:16'),('3e229e816e325b0863e11733fea79f174798d182888d1aee2570601d0d6d90994d0d3a2a8fe1fac8',2,3,NULL,'[]',0,'2018-07-25 20:53:12','2018-07-25 20:53:12','2018-07-26 15:53:12'),('539e19702b2f5e27676b94ef9cc992b7f23c51f88aa081c7de3b8fc4c5f14ac0b2f5eb6cc78708c8',1,3,NULL,'[]',0,'2018-06-09 20:23:56','2018-06-09 20:23:56','2018-06-10 20:23:56'),('66794f3a669c773481a85219b46bd4ce43e0e4f301ee67ca19c36b61f9edcb9c3be6a3e7ef2c50bd',1,3,NULL,'[]',0,'2018-06-09 07:40:42','2018-06-09 07:40:42','2019-06-09 02:40:42'),('7015fd2cb4ef0bad5c4eb5aebc68c6f5c9088060485d2c3e2ae2e75ba413cca96b5fa2be73f4befc',1,3,NULL,'[]',0,'2018-06-09 07:46:31','2018-06-09 07:46:31','2019-06-09 02:46:31'),('712cba305e22f7d696b02ca43ea1b419603d6acd628dcb49422a73d3faf68f3267e67cc15bcc22f0',1,3,NULL,'[]',0,'2018-06-09 07:45:09','2018-06-09 07:45:09','2019-06-09 02:45:09'),('7c6855588452eff87ae235a4322d646a57b8205a20e84a0ec98d8e9a9cd6e7b86c8742f0cf303469',1,3,NULL,'[]',0,'2018-06-09 07:42:26','2018-06-09 07:42:26','2019-06-09 02:42:26'),('7f0b67e979865f28e8996c7db8ee781e29fc8bf9271f757ff4585e354aa3ea5dce5625ea283ada93',2,3,NULL,'[]',0,'2018-07-25 09:38:38','2018-07-25 09:38:38','2018-07-26 04:38:38'),('8577a7aa00ca10433a7b34dd731e584eb8d0e2cbb236077fe3bfbaf5aaea8c2ba9429970f66e1bb5',1,3,NULL,'[]',0,'2018-06-09 07:43:19','2018-06-09 07:43:19','2019-06-09 02:43:19'),('900a6822c0baff8a8b8e7f4871d1137adff6403fa5ba20f41a234ed8157de45342f3747c7669258d',1,3,NULL,'[]',0,'2018-06-09 07:48:08','2018-06-09 07:48:08','2019-06-09 02:48:08'),('96ffa8726624985e8cf84e9117c3d66d781d0df7cba4aea52b264693d6f76204ead08a770ea2b779',1,3,NULL,'[]',0,'2018-06-09 04:22:28','2018-06-09 04:22:28','2019-06-08 23:22:28'),('99cb7487d313bd5c8f09e301a5ac140f6c4a63c288c43305a0dcf05414457ebf8ac1af0d76046b6d',1,3,NULL,'[]',0,'2018-06-09 04:07:37','2018-06-09 04:07:37','2019-06-08 23:07:37'),('9e7f343a3428ea33e21efd10f7270f3f9117d7816049df484a1d17abe0545cb18c05fc4f0128d7eb',2,3,NULL,'[]',0,'2018-07-26 05:06:24','2018-07-26 05:06:24','2018-07-27 00:06:24'),('a5f39c7f8d95634d1d14407e412c11b47c4f588abde86ccafb256ebc8d32575a9354db438c9f9981',3,3,NULL,'[]',0,'2018-06-09 09:54:18','2018-06-09 09:54:18','2019-06-09 04:54:18'),('ab769ce0f8ea457a6effce826418668f9327f4114ea9a8c79a4716c3a8098ada079c5d31d8538575',1,3,NULL,'[]',0,'2018-06-09 07:41:36','2018-06-09 07:41:36','2019-06-09 02:41:36'),('bc93855c195cd4703e7a2d7d29029a23a030bc4a89790e01f3914d30a4cae0baf418bb2fc85f57d3',1,3,NULL,'[]',0,'2018-06-09 18:26:38','2018-06-09 18:26:38','2018-06-10 18:26:38'),('beab8043d13c6e9658261640a0c23dfe43107e9a2304e884e4e534746c8f5a8562358b04754b40bb',5,3,NULL,'[]',0,'2018-06-09 18:59:03','2018-06-09 18:59:03','2018-06-10 18:59:03'),('cc52aed649cb4676accda89dcac1d8527aa7731bf85845ac854445e4d0468b4b53f02bff1cddcc0a',1,3,NULL,'[]',0,'2018-06-09 07:45:36','2018-06-09 07:45:36','2019-06-09 02:45:36'),('da1e4e04069e5a85057102530655f4f21d653faa11e502791c2165be6eff97f8ae55a62735346ac3',1,3,NULL,'[]',0,'2018-06-09 07:42:04','2018-06-09 07:42:04','2019-06-09 02:42:04'),('e1253b1ff56f9db9166936c51c44ca59a4f07f7f6c2a9bce6a923adeeebf78930600756ecdc24d7f',1,1,'fullstack','[]',0,'2018-06-09 18:04:52','2018-06-09 18:04:52','2019-06-09 18:04:52'),('e4ee72b59a2a82482236d603abbdd15ab326768302f08fc44e6a9ec08a803dad9fdff20354e8ba23',6,3,NULL,'[]',0,'2018-06-11 13:54:24','2018-06-11 13:54:24','2018-06-12 13:54:24');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
INSERT INTO `oauth_auth_codes` VALUES ('00196e826d5f677b4c7a5897e74760cbbec1cc95ce44a31c55299d1a000c2819d4dc6e832e4dc921',6,3,'[]',1,'2018-06-11 14:04:22'),('0048d40afac12d97cb9c9d5b97723c8762fb3955bce484b5940dffee9a86356ee144f97d654103ea',1,3,'[]',1,'2018-06-09 02:56:31'),('080f6c94105ad6319044ea3b8fa2e81f9f8ac9c7e30d6d523546196de83fb46bac8b2217c2924be8',3,3,'[]',1,'2018-06-09 05:04:17'),('27290a6f44690d8bc8a6b484e42eb353407a789ce889e535a85c468fe511ab2e8ecc521404ec63e2',1,3,'[]',1,'2018-06-09 20:49:16'),('28349f5c55b7de7d6fba8c0485e24b387f229f2743e69a27b3b0cfdf361b44b88725789aa82a6810',2,3,'[]',1,'2018-07-26 00:16:24'),('2b5160a92500cb3bb606888c25a295c7295750c4622ee3cf24402becedf200ecadc6441b61398f63',1,3,'[]',1,'2018-06-09 18:36:37'),('32959c52e069b8fb27e47e95f45144dbce2cd9e1260b112e54b086666e7f2f983bf6a861dce89694',1,3,'[]',1,'2018-06-09 20:33:55'),('3838e66e7923c3817a6c6e4032825c6953a6cf49328ab207f4d7adb9c617d9471b3db1a9671380f2',1,3,'[]',1,'2018-06-09 02:52:26'),('3f9db60e7d583eab1c9f8e52b4891cb742dff23829e6bedf034929be64632cbea83a7597f0431cb9',1,3,'[]',1,'2018-06-09 02:53:18'),('49406cd698fe4c2dfdd755c741bb24b7a6d92d7fa11d792e3dfca42aa2ad5b4060e672d424f7184b',1,3,'[]',1,'2018-06-08 23:32:27'),('53b2b05372bef3e4a57d622ae4252a87965c00ec19b830d0d579127d8bf39bfca3bca48496b317fe',1,3,'[]',1,'2018-06-09 02:52:04'),('5af8eba81a719e8dc0506ce28c5dd07aa8a6156a3f71d1a9850ef57cce08c43f488505513ae4d88f',2,3,'[]',1,'2018-07-25 16:03:11'),('6113d251643edb651ba63e0cdda582ab945bef3108e6f82a339cd67ac077e182cdb002dbe343a8c0',1,3,'[]',1,'2018-06-08 23:14:42'),('71b19b69de14e5666f969d7fcb54cc4f7bc5a249750719d7ed897460b25b895bdc41607d3ab172a8',1,3,'[]',1,'2018-06-09 02:50:41'),('8275ef9d38a91c7e97ca3a140340b38f2e35b944473eadafcd1ce57be0e63df774fd1f1a2c86c9f0',2,3,'[]',1,'2018-06-09 21:00:19'),('87ac17ea9e2afd5e29413f143d2165387cac1e4fc0a309640d69dc0e37ccbc908acffd2bac1693fd',1,3,'[]',1,'2018-06-09 02:55:36'),('af75315094d1d59425d92e860a7285182dfedb84a1c599871126ce584644dd5735e6b87b414d2414',2,3,'[]',1,'2018-07-25 04:48:38'),('de2c3be30596cf4176acd28ef07f6d3fa9a33ac2594fe154f9bd759dd53f0094e80adc43aa132554',1,3,'[]',1,'2018-06-09 02:51:35'),('e086ad0adbca757f7bb5a1059d8144c89c0efa367b3b7a289db6802df80484dd8561c50e5f462f8b',1,3,'[]',1,'2018-06-09 02:55:09'),('e5d0d201211a6ef8aa2a775b1e46f114c8f5122bcb27e2b7c810843c97683be14c2789995e445551',1,3,'[]',1,'2018-06-09 18:33:29'),('ecd0267f6fd29add1e6dfb9bfe206a3a7822eb6ef384dc109d9db92474caf23ad09a0141815f2ae7',1,3,'[]',1,'2018-06-09 02:54:07'),('f2545429d503118a5d3862ec1edc613201faf393f5affddb06c943418292cc630db7932daa3be35e',5,3,'[]',1,'2018-06-09 19:09:01'),('fbdd062639a8da83a1ac686f8aea5f671e423614c85c063c66089d5f0fe3dfb876eea0d835dcc709',1,3,'[]',1,'2018-06-09 02:58:08'),('ff8f130012fcecca326e633524a9ffd97549948f97b87b627ce8feff8e0a99e586fd8ae1d8fdd3d2',1,3,'[]',1,'2018-06-08 23:31:05');
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','LBlII4XnuqvirOOnjCSpwAcnKdh2wZAPNaNcge0k','http://localhost',1,0,0,'2018-06-09 02:52:14','2018-06-09 02:52:14'),(2,NULL,'Laravel Password Grant Client','pkuX5c2boNxNtYo8MC3ykjKRTxOZt42pLvS6Ew53','http://localhost',0,1,0,'2018-06-09 02:52:14','2018-06-09 02:52:14'),(3,1,'Full-stack APP','xXUTrUETSyVRPEKk79aWipCzrmIH31avkDJfnA4h','http://fullstack.test/callback',0,0,0,'2018-06-09 03:46:10','2018-06-09 03:46:10');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2018-06-09 02:52:14','2018-06-09 02:52:14');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('06080282bdba71ca2edef11e6bbd5096e31ffb0bc412329f99b54f0c3f49db95195520f9bec46694','8577a7aa00ca10433a7b34dd731e584eb8d0e2cbb236077fe3bfbaf5aaea8c2ba9429970f66e1bb5',0,'2019-06-09 02:43:19'),('072f8e27e3ff9825d66d7329c31239cbc99f2213d2b657682931ca84a4fa20e08f7a0cd9ae556162','7015fd2cb4ef0bad5c4eb5aebc68c6f5c9088060485d2c3e2ae2e75ba413cca96b5fa2be73f4befc',0,'2019-06-09 02:46:31'),('10df0c6aa8d5effece8410daf5cad5f727ac30f023577a962ccf7525bae80294a30b912c25130104','99cb7487d313bd5c8f09e301a5ac140f6c4a63c288c43305a0dcf05414457ebf8ac1af0d76046b6d',0,'2019-06-08 23:07:37'),('13e5ea35c72daa6d2928512a002ccb4cb17bc8f239a13aaf9c9e4bf23813ac477eb1bac6e1b15642','539e19702b2f5e27676b94ef9cc992b7f23c51f88aa081c7de3b8fc4c5f14ac0b2f5eb6cc78708c8',0,'2018-06-10 20:23:56'),('17c05d27f395f096d20d37528fa6571af0ebec85ffb969d88c8c31b594b14daf461aae13dea2e15d','2f91be97de5f7efa647f9edfa419dbc6637d82f288960b382c3f30329cea5412500c9cf6e857ad68',0,'2018-06-10 20:39:16'),('1ec477aa6940bdabd4cb7e3daaf14ab7b2d13b46e5f1470dcb11bcaa2d5f3f83b0b436330c53cf84','beab8043d13c6e9658261640a0c23dfe43107e9a2304e884e4e534746c8f5a8562358b04754b40bb',0,'2018-06-10 18:59:03'),('1fcba84529d3afcb9e4b8ada7dcaf430daba866047b4aacb235146bd0852c8127e9d664b6467216b','9e7f343a3428ea33e21efd10f7270f3f9117d7816049df484a1d17abe0545cb18c05fc4f0128d7eb',0,'2018-07-27 00:06:24'),('378ff3f35aff282dbc3094be9f00dd3eacba1e5fa3b827678c9c19e1ea5c56d76aca2d71083ee3bc','2c805e5e30713a824782b7d5425246508937aaf4903ee9481634cc1ed50e1b8cb00a32c5b26b50b8',0,'2018-06-10 18:23:30'),('3bf02c597e458616c139a4840cfd18ee28e3cfd908e0abe9c1f37164df8af2130dc429dba52afe78','24447114125c0f918cb2468f55dba33204918fc2b607070ca9679239e7fb173506f4ce2651a7f31c',0,'2019-06-09 02:44:07'),('4160d7d6a14ed8f648e02c291282e948ac13b8660c91d9196a5f71dfa27ae96affd0beac47f94ba1','7c6855588452eff87ae235a4322d646a57b8205a20e84a0ec98d8e9a9cd6e7b86c8742f0cf303469',0,'2019-06-09 02:42:26'),('508a7536317dacea2f4445df34338d61ed0ae47a32f40b022ef5076b19d57084fced4a26f9590551','96ffa8726624985e8cf84e9117c3d66d781d0df7cba4aea52b264693d6f76204ead08a770ea2b779',0,'2019-06-08 23:22:28'),('52611505a0d12e66eb020886a12f3e3d81a8e0d9a214735114c5c9d769d626617c4a5ccbe7c5e9b0','1956465e3e7f791a5d2abe7b223e66912778babf4e49308b02396e4252bc691e9f056a7529cb91d6',0,'2019-06-08 23:21:05'),('5323fbeab93a864d69fe137d43960b000c97f6cc2250b1afa9a96ae8489d248f9e2b8b07f2674660','ab769ce0f8ea457a6effce826418668f9327f4114ea9a8c79a4716c3a8098ada079c5d31d8538575',0,'2019-06-09 02:41:36'),('71cd5d2f18d2a06f58e17457ecea21866fbcf48cc30a17e8049f69904ee8e8c75fc6867a71ca18f9','7f0b67e979865f28e8996c7db8ee781e29fc8bf9271f757ff4585e354aa3ea5dce5625ea283ada93',0,'2018-07-26 04:38:38'),('7972e6905325a3056a16172e09b3504faf2b140c558e87a28b07cfd550e0069a3e9bd3971d9f3384','bc93855c195cd4703e7a2d7d29029a23a030bc4a89790e01f3914d30a4cae0baf418bb2fc85f57d3',0,'2018-06-10 18:26:38'),('7be58ba036ba9aa81206c843e4a60029771a1492cc01934ef849f94a7c3a522d5675bcc636229445','900a6822c0baff8a8b8e7f4871d1137adff6403fa5ba20f41a234ed8157de45342f3747c7669258d',0,'2019-06-09 02:48:08'),('84472f81023a35d735ec06a7160067069830d753e193c3cd84d3f843ffcc68ebb92c793593648d9b','cc52aed649cb4676accda89dcac1d8527aa7731bf85845ac854445e4d0468b4b53f02bff1cddcc0a',0,'2019-06-09 02:45:36'),('8680e9320365d0208ba221c93b444eafbbdfb825941bb020353df7e56666139e1ed82b1d4e6d6669','e4ee72b59a2a82482236d603abbdd15ab326768302f08fc44e6a9ec08a803dad9fdff20354e8ba23',0,'2018-06-12 13:54:24'),('a34961b7f8bfb5a9fc8dabcb548e3a1af6ea7982e4b0084d4c94dc0d711352da350ff9445566ac9f','a5f39c7f8d95634d1d14407e412c11b47c4f588abde86ccafb256ebc8d32575a9354db438c9f9981',0,'2019-06-09 04:54:18'),('bd9eb4764876b45b4b71d4607c6f4ccc3b5d125fd607c50a4a5576ca746ae45db42727fe9ce9f397','2c9f78306228ddaa593eba4f8847e38b3a9b0e22bf2a11afce13e8d5dfef2bf31259fe19b7ceea06',0,'2018-06-10 20:50:19'),('c3c695b13d82d5b51b191bde4c3db6667ed2cc450665ce9ca4d1b9b01b64e7625eb35572819a83c7','da1e4e04069e5a85057102530655f4f21d653faa11e502791c2165be6eff97f8ae55a62735346ac3',0,'2019-06-09 02:42:04'),('c7451599cab114c6f522667c3d70b776215b1e0f9ee489becaf64fafc90cea86ef163cc8d4dfafce','712cba305e22f7d696b02ca43ea1b419603d6acd628dcb49422a73d3faf68f3267e67cc15bcc22f0',0,'2019-06-09 02:45:09'),('ca66dd979491365e3d6ba35b85f3bbbc2c63224af319973df1dcfcda773942b79e6e1a00e0bd6d2e','3e229e816e325b0863e11733fea79f174798d182888d1aee2570601d0d6d90994d0d3a2a8fe1fac8',0,'2018-07-26 15:53:12'),('fbe9f5c8b27e446559e076709b9152da5deef0479cf22f1a1529755462c889b40c2682fac3489ff4','66794f3a669c773481a85219b46bd4ce43e0e4f301ee67ca19c36b61f9edcb9c3be6a3e7ef2c50bd',0,'2019-06-09 02:40:42');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realestate_features`
--

DROP TABLE IF EXISTS `realestate_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realestate_features` (
  `realestate_id` int(10) unsigned NOT NULL,
  `feature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `realestate_features_realestate_id_foreign` (`realestate_id`),
  CONSTRAINT `realestate_features_realestate_id_foreign` FOREIGN KEY (`realestate_id`) REFERENCES `realestates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realestate_features`
--

LOCK TABLES `realestate_features` WRITE;
/*!40000 ALTER TABLE `realestate_features` DISABLE KEYS */;
INSERT INTO `realestate_features` VALUES (18,'car 1'),(18,'car 3'),(18,'nuevo car 2'),(17,'Verde'),(17,'Bonito'),(17,'Elegante'),(17,'Atrativo');
/*!40000 ALTER TABLE `realestate_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realestates`
--

DROP TABLE IF EXISTS `realestates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realestates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neighborhood` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery` text COLLATE utf8mb4_unicode_ci,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `realestates_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realestates`
--

LOCK TABLES `realestates` WRITE;
/*!40000 ALTER TABLE `realestates` DISABLE KEYS */;
INSERT INTO `realestates` VALUES (17,'PCOM-HIR/72','Casa bonita','DF','Ciudad de México','Cuauhtémoc','Tlatelolco,','Paseo de La Reforma Nte 680 Cuauhtémoc, Tlatelolco,','[\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/1440x1920\\/56064461_YxAB0ABcO_HZAuJHkGzs_v9E07ilXjtAB0G3z-0-U28.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/1440x1920\\/6070583_uX9AfIfeeDTeFbPG2D0yL6AJa4QEgqdTZo2gMtnJG-I.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/540x960\\/68117177_4VDT3ML1x0jl_rZrQP8pGaqKeli6UB1_D6PowQ_o3Jk.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/959x717\\/18851494_b3iM750eQIcIGLLJdZBdqtocY5iEMuOcuNDCdv-OuIY.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/960x720\\/38427868_4HDvq5uOWW4MRmgp296yF5eb4rRM_f0Zu-Y-8qBW-30.jpg\"]',19.45117540,-99.13397310,'2018-07-25 19:45:03','2018-07-26 05:07:37'),(18,'PCOM-PDM/62','Palácio test editado 2','DF','Ciudad de México','Cuauhtémoc','Colonia Centro (Área 1)','Calle Plaza de la Constitución 1, Piso 1','[\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/3684x2308\\/60326320__UsubmYZVac28Tl4SNGVAPQndMwSa4Yj1FfmHRo6XCE.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/540x720\\/WMiIcP--PIk0QxYrRAv3tqeNpVeYbSWYDxBTOUVbg-E.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/1920x1440\\/63291314_4yd7uhiZTwl8PiwBW8XxzuIBr2ve36Hh12lSuYuGGtM.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/1920x1439\\/57923168_h1dPo8hniTCOL2x1nMjQqu44Z0fDUVTmhe46vdSIc5o.jpg\",\"https:\\/\\/igx.4sqi.net\\/img\\/general\\/1068x1900\\/8770676_-RPguyreDeSKXKIJa4anYVeWSqBvhGbyHLdd2Z5K_es.jpg\"]',19.43239030,-99.13424710,'2018-07-25 19:45:26','2018-07-25 21:01:11');
/*!40000 ALTER TABLE `realestates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@admin.com','$2y$10$EttySElwc9eXNYeDGmUJ4O7CMy18Iw78Dtm7PYjAK60cqdMOKoMvi','UiJLMHnKt7IIqjYoWaj4CinXNoMMMM8Pk4ndGCauXFZxT5Fbk1NTBmqDfsfv',NULL,NULL),(2,'Lucas Serafim','lslucas@gmail.com','$2y$10$NMUdVtXooNIQ8l9HNkrgX.SE8W2ZryOxqRkgLGLkL.ttH.lWs9mfC','IvUiKgDQzTx1RKOuxJLqnuCh87yEsDh9Qh2GoR9ooQU1Ykvgb9By0wIIedtc','2018-06-09 09:53:30','2018-06-09 20:50:17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'Testing_fullstack'
--

--
-- Dumping routines for database 'Testing_fullstack'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-25 19:30:08
