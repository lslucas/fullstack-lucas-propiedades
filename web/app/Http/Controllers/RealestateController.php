<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FourSquareApiService;
use App\Services\MapService;

class RealestateController extends Controller
{
    protected $foursquare;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:api');

        $this->foursquare = new FourSquareApiService;
        $this->maps = new MapService;
    }

    /**
     * Show a list of github users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->page ?: 1;
        $since = $request->since ?: '';
        $per_page = $request->per_page ?: 100;

        // need to fix the pagination
        $response = \App\Realestate::orderBy('id', 'desc')->paginate($per_page);

        return response()->json($response);
    }

    public function map(Request $request)
    {
        $response = \App\Realestate::orderBy('id', 'desc')->get();

        $res = [];

        foreach ($response as $resp) {
            $gal = json_decode($resp->gallery, true);

            $html = '<h3>'. $resp->name .'</h3>';
            $html .= '<p>Delegación: '. $resp->district;
            $html .= '<br/>Galeria de fotos:<br/>';

            foreach ($gal as $pic) {
                $html .= '<div class="img-thumb"><a href="'. $pic . '" target="_blank"><img src="'. $pic . '" class="rounded float-left" border=0 width=80/></a></div>';
            }
            $html .= '</p><br/><a href="/realestate/show?code='.$resp->code.'" class="btn btn-info">Más detalles</a>';

            $res[] = [
                'title' => $resp->name,
                'position' => [
                    'lat' => $resp->lat,
                    'lng' => $resp->lng
                ],
                'icon' => 'parking',
                'content' => $html 
            ];
        }

        return response()->json($res);
    }

    /**
     * Show info about a github user.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $response = \App\Realestate::where('code', $request->code)->first();

        $features = \App\RealestateFeature::where('realestate_id', $response->id)->pluck('feature');

        $response->features = $features->toArray();

        return response()->json($response);
    }

    /**
     * Show the repos of a github user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $payload = $request->only(['name', 'city', 'state', 'district', 'neighborhood', 'street']);
        $features = $request->only(['features']);

        $response = $this->storeUpdate($payload, $features);

        return response()->json($response);
    }

     /**
     * Show the repos of a github user.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $payload = $request->only(['id', 'name', 'city', 'state', 'district', 'neighborhood', 'street']);
        $features = $request->only(['features']);

        $response = $this->storeUpdate($payload, $features);

        return response()->json($response);
    }   

    public function delete($id)
    {
        $realestate = \App\Realestate::find($id);

        if (!$realestate) {
            return response()->json(['message' => 'Item no existe'], 404);
        } else {
            \App\RealestateFeature::where('realestate_id', $id)->delete();

            $realestate->delete();
            return response()->json([], 201);
        }
    }

    public function storeUpdate($payload, $features)
    {
        if (isset($payload['id'])) {
            $realestate = \App\Realestate::find($payload['id']);
        } else {
            $realestate = new \App\Realestate;
        }

        $payload['code'] = $realestate->newCode();

        $maps = $this->maps->textsearch($payload);

        if (!isset($maps[0])) {
            return false;
        }

        $payload['lat'] = $maps[0]['coordinates']['lat'];
        $payload['lng'] = $maps[0]['coordinates']['lng'];
        $payload['gallery'] = $this->foursquare->getPhotos($maps[0]['coordinates']);

        if (isset($payload['id'])) {
            $response = $realestate;

            $items = $payload;
            unset($items['id']);

            foreach ($items as $key=>$item) {
                $realestate->$key = $item;
            }

            $realestate->save();

        } else {
            $response = $realestate->create($payload);
        }

        // features pivot
        \App\RealestateFeature::where('realestate_id', $response->id)->delete();
        foreach ($features as $_feature) {
            foreach ($_feature as $feature) {
                if (!empty($feature)) {
                    $featureBulk = ['realestate_id' => $response->id, 'feature' => $feature];
                    \App\RealestateFeature::create($featureBulk);
                }
            }
        }

        return $response;
    }
}
