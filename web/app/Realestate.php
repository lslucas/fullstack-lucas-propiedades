<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RealestateFeature;

class Realestate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'state', 'city', 'district', 'neighborhood', 'street', 'gallery', 'lat', 'lng'
    ];

    /**
     * Generate a new unique code based on pattern: PCOM-XXX/##
     *
     * @return string
     */
    public function newCode()
    {
        $prefix = 'PCOM-';

        do {
            $randomText = $this->randomLetters();
            $randomNumber = sprintf('%02d', mt_rand(0, 99));

            $code = $prefix . $randomText . '/' . $randomNumber;
        } while ($this->uniqueCode($code));

        return strtoupper($code);
    }

    /**
     * Test if the code already exists or not
     *
     * @param [type] $code
     * @return bool
     */
    public function uniqueCode($code)
    {
        return (bool) $this->where('code', $code)->count();
    }

    /**
     * Relationship with features our pivot table
     *
     * @return model
     */
    public function features()
    {
        return $this->hasMany(RealestateFeature::class);
    }

    /**
     * Generate random letters
     *
     * @param integer $limit
     * @return string
     */
    public function randomLetters($limit=3)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $result = '';
        for ($i = 0; $i < $limit; $i++) {
            $result .= $characters[mt_rand(0, strlen($characters)-1)];
        }

        return $result;
    }
    
}
