<?php namespace App\Services;

class MapService
{
     /**
     * When sending a string of an location, exe: Polanco, México, will return a json with the lat/lng
     * @param      <string>  $q      Location
     * @return     <json>  ( lat/lng )
     */
    public function textsearch($payload=[])
    {
        $key = env('GOOGLE_API_KEY', 'AIzaSyBne0BqiOTU9K066pBoZSio13LkpO5E1zA');

        $baseURL = 'https://maps.googleapis.com/maps/api/place/textsearch/json?key='.$key;

        // tratamiento de la query
        $location = $payload['street'] . ', ' . $payload['neighborhood'] . ', ' . $payload['district'] . ', Mexico';
        $address = trim($location);

        if (!$address) {
           return false;
        }

        $mapsurl = $baseURL . '&query=' . urlencode($address) . "&language=es-MX";

        $output = file_get_contents($mapsurl);
        $response = json_decode($output);
        $res = $json = [];

        if (@$response && $response->status=='OK' && count($response->results)) {
            foreach ($response->results as $k => $ret) {

                @list($address, $number_neightboard, $city_state, $zip, $country) = explode(',', $ret->formatted_address);
                @list($number, $neigthboard) = explode(' - ', $number_neightboard);
                @list($city, $state) = explode(' - ', $city_state);

                $res[$k]['address']['full'] = $ret->name;
                $res[$k]['address']['formatted'] = trim($address);
                $res[$k]['address']['number'] = trim($number);
                $res[$k]['address']['state'] = trim($state);
                $res[$k]['address']['countryRegion'] = trim($country);
                $res[$k]['address']['locality'] = trim($city);
                $res[$k]['address']['postalCode'] = trim($zip);
                $res[$k]['address']['neighborhood'] = trim($neigthboard);
                $res[$k]['coordinates']['lat'] = $ret->geometry->location->lat;
                $res[$k]['coordinates']['lng'] = $ret->geometry->location->lng;
            }

            $location = count($res)==1 ? 'location' : 'locations';
            $json['msg'] = count($res).' '.$location.' found!';
            $json['result'] = $res;
            $json['code'] = 200;

        } elseif ($response->status=='REQUEST_DENIED') {
            $json['msg'] = $response->error_message;
            $json['url'] = $mapsurl;
            $json['code'] = 203;
            $json['result'] = $response;
            print_r($json);
            exit;

        } else {
            $json['msg'] = 'No locations found!';
            $json['code'] = 202;
            $json['result'] = $response;
        }

        return $json['result'];
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000
    ) {
        if ( empty($latitudeTo) || empty($longitudeTo) || empty($latitudeFrom) || empty($longitudeFrom) )
            return false;

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return number_format((($angle * $earthRadius)/1000), 3); // converted to km
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit='K')
    {
        if (empty($lat1) || empty($lon1) || empty($lat2) || empty($lon2)) {
            return false;
        }

        if (!is_numeric($lat1) || !is_numeric($lon1)) {
            return false;
        }

        // php converts long floats to string, so we need to force as float to avoid bugs
        $lat1 = (float) $lat1;
        $lat2 = (float) $lat2;
        $lon1 = (float) $lon1;
        $lon2 = (float) $lon2;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            $val = number_format(($miles * 1.609344), 3);
        } else if ($unit == "N") {
            $val = ($miles * 0.8684);
        } else {
            $val = $miles;
        }

        return str_replace(',', '', $val);
    }

}