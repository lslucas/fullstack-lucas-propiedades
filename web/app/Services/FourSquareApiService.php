<?php namespace App\Services;

use GuzzleHttp\Client;

class FourSquareApiService
{
    public $client;

    public function __construct()
    {
        $this->startup();

        return $this->client;
    }

    public function startup()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.foursquare.com/v2/',
            'timeout'  => 10,
        ]);
    }

    /**
     * Get an endpoint from foursquare api
     *
     * @param [type] $endpoint
     * @return void
     */
    public function get($endpoint)
    {
        // if we have a github access token we shall use it :)
        $client_id = env('FOURSQUARE_CLIENT_ID');
        $client_secret = env('FOURSQUARE_CLIENT_SECRET');
        $token = '';

        $token = '&client_id=' . $client_id . '&client_secret='. $client_secret;

        $rq = $this->client->request('GET', $endpoint . $token);

        return [
            'code' => $rq->getStatusCode(),
            'data' => json_decode($rq->getBody()->getContents())
        ];
    }

    /**
     * Get photos based of lat/long
     *
     * @param [string] $ll  lat,long
     * @return json gallery with 5 items
     */
    public function getPhotos($ll)
    {
        $resp = $this->get('venues/search?ll=' . join(',', array_values($ll)) . '&v=20180725');

        $items = [];

        if (!$resp) {
            return false;
        }

        foreach ($resp['data']->response->venues as $venues) {
            $items[] = $venues;
        }
        
        $gal = [];
        foreach ($items as $item) {
            $photos = $this->get('venues/' . $item->id . '/photos?v=20180725');

            if (!$photos) {
                return false;
            }

            foreach ($photos['data']->response->photos->items as $photo) {
                $size = $photo->width. 'x'. $photo->height;
                $gal[] = $photo->prefix . $size . $photo->suffix;
            }

            if (count($gal)==5) {
                break;
            }
        }
        
        return json_encode($gal);
    }
}