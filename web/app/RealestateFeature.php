<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealestateFeature extends Model
{
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'realestate_id', 'feature'
    ];

}
