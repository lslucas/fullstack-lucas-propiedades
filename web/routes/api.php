<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authorize', 'ApiController@auth')->name('authorize');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    [
        'prefix' => 'realestate',
        //'middleware' => 'auth:api'
    ], function () {

        Route::get('map', 'RealestateController@map')->name('realestate.map');
        Route::get('', 'RealestateController@index')->name('realestate.index');
        Route::post('', 'RealestateController@create')->name('realestate.create');
        Route::get('show', 'RealestateController@show')->name('realestate.show');
        Route::put('/{id}', 'RealestateController@update')->name('realestate.update');
        Route::delete('/{id}', 'RealestateController@delete')->name('realestate.delete');

});