## About this project

It was made with Laravel as our backend. API authentication is using oAuth2 and the front for Github users with vuejs.

## Extras

- Postman [API](https://documenter.getpostman.com/view/178818/RWMJqSiL#518ebdd7-5984-4aa1-abfa-0ba0f2157006).
